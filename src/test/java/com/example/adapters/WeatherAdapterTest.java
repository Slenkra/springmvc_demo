package com.example.adapters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;

import org.junit.BeforeClass;
import org.junit.Test;

import com.example.domain.RequestModel;
import com.example.domain.Weather;
import com.example.services.WeatherService;
import com.example.util.WeatherManipulator;

public class WeatherAdapterTest {

	private static final String VALID_CITY_NAME = "Makó";
	private static final String VALID_WEATHER_CELSIUS = "13.1";
	private static final String VALID_WEATHER_KELVIN = "-362.9";
	private static final String INVALID_CITY_NAME = "";
	private static final String INVALID_WEATHER = null;

	static WeatherService weatherService;

	static WeatherManipulator weatherManipulator;


	static WeatherAdapter weatherAdapter;

	@BeforeClass
	public static void setUp() {
		weatherService = mock(WeatherService.class);
		weatherManipulator = mock(WeatherManipulator.class);
		weatherAdapter = new WeatherAdapter();
		
		weatherAdapter.weatherManipulator = weatherManipulator;
		weatherAdapter.weatherService = weatherService;
	}

	@Test
	public void testHappyPathWithCelsius() throws URISyntaxException {

		when(weatherService.getCurrentWeather(VALID_CITY_NAME)).thenReturn(stubWeatherHappyCase());

		Weather weather = weatherAdapter.retriveWeather(stubValidRequest(false));
		assertNotNull(weather);
		assertEquals(VALID_CITY_NAME, weather.getCityName());
		assertEquals(VALID_WEATHER_CELSIUS, weather.getTemperature());
	}

	@Test
	public void testHappyPathWithKelvin() throws URISyntaxException {

		when(weatherService.getCurrentWeather(VALID_CITY_NAME)).thenReturn(stubWeatherHappyCase());
		when(weatherManipulator.convertToKelvin(VALID_WEATHER_CELSIUS)).thenReturn(VALID_WEATHER_KELVIN);

		Weather weather = weatherAdapter.retriveWeather(stubValidRequest(true));
		assertNotNull(weather);
		assertEquals(VALID_CITY_NAME, weather.getCityName());
		assertEquals(VALID_WEATHER_KELVIN, weather.getTemperature());
	}
	
	@Test
	public void testUnHappyPathWithCelsius() throws URISyntaxException {

		when(weatherService.getCurrentWeather(VALID_CITY_NAME)).thenReturn(stubWeatherUnHappyCase());

		Weather weather = weatherAdapter.retriveWeather(stubValidRequest(false));
		assertNotNull(weather);
		assertEquals(VALID_CITY_NAME, weather.getCityName());
		assertEquals(INVALID_WEATHER, weather.getTemperature());
	}
	
	private Weather stubWeatherHappyCase() {
		return new Weather("13.1", "Kecskemét");
	}

	private Weather stubWeatherUnHappyCase() {
		return new Weather(null, null);
	}
	
	private RequestModel stubValidRequest(boolean isKelvin){
		RequestModel requestModel = new RequestModel();
		requestModel.setName(VALID_CITY_NAME);
		requestModel.setKelvin(isKelvin);
		
		return requestModel;
	}
	
	private RequestModel stubInValidRequest(boolean isKelvin){
		RequestModel requestModel = new RequestModel();
		requestModel.setName(INVALID_CITY_NAME);
		requestModel.setKelvin(isKelvin);
		
		return requestModel;
	}

}
