package com.example.domain;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Weather {

	private String temperature;

	@JsonIgnore
	private String cityName;

	public Weather() {

	}

	public Weather(String temperature, String cityName) {
		super();
		this.temperature = temperature;
		this.cityName = cityName;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	@JsonProperty("main")
	public void setTemp(Map<String, Object> main) {
		setTemperature(main.get("temp").toString());
	}

}
