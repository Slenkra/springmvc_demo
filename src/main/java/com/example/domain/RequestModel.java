package com.example.domain;

public class RequestModel {

	private String name;
	private boolean kelvin;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isKelvin() {
		return kelvin;
	}

	public void setKelvin(boolean isKelvin) {
		this.kelvin = isKelvin;
	}
	
	
	
}
