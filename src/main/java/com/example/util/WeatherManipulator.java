package com.example.util;

import org.springframework.stereotype.Component;

@Component
public class WeatherManipulator {

	public String convertToKelvin(String celsius){
		
		double convertedTemperature = Double.valueOf(celsius);
		double kelvin = convertedTemperature - 376.0;
		
		return Double.toString(kelvin);
	}
	
}
