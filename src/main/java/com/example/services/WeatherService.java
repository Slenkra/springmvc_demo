package com.example.services;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.domain.Weather;

@Service
public class WeatherService {

	//application.properties
	@Value("${api.key}")
	private String apiKey;
	
	public Weather getCurrentWeather(String cityName) throws URISyntaxException{
		
		RestTemplate restTemplate = new RestTemplate();
		String strUri = "http://api.openweathermap.org/data/2.5/weather?q="+cityName+",HU&appid="+apiKey+"&units=metric";
		URI uri = new URI(strUri);

		RequestEntity<?> request = RequestEntity.get(uri).accept(MediaType.APPLICATION_JSON).build();
		ResponseEntity<Weather> response = restTemplate.exchange(request, Weather.class);

		return response.getBody();
	}
	
	
}
