package com.example.adapters;

import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.domain.RequestModel;
import com.example.domain.Weather;
import com.example.services.WeatherService;
import com.example.util.WeatherManipulator;

@Component
public class WeatherAdapter {

	@Autowired
	WeatherService weatherService;
	
	@Autowired
	WeatherManipulator weatherManipulator;
	
	public Weather retriveWeather(RequestModel request){
		
		Weather currentWeather = null;
		
		try {
			currentWeather = weatherService.getCurrentWeather(request.getName());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		currentWeather.setCityName(request.getName());

		if(request.isKelvin()){
			String kelvin = weatherManipulator.convertToKelvin(currentWeather.getTemperature());
			currentWeather.setTemperature(kelvin);
		}
		
		return currentWeather;
	}
	
}
