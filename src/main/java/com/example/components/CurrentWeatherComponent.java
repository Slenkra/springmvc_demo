package com.example.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.domain.Weather;
import com.example.services.WeatherService;

@Component
public class CurrentWeatherComponent {

	public Weather prepareWeatherData(){
		return new Weather("10", "Szeged");
	}
	
	
}
