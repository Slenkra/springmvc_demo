package com.example.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.adapters.WeatherAdapter;
import com.example.domain.RequestModel;

@Controller
public class HelloWorldController {

	@Autowired
	WeatherAdapter weatherAdapter;

	@RequestMapping(value = "/weather", method = RequestMethod.GET)
	public ModelAndView helloWorld() {

		return new ModelAndView("weather", "requestModel", new RequestModel());

	}

	@RequestMapping(value = "/weather/search", method = RequestMethod.POST)
	public String getCityWeather(@ModelAttribute("requestModel") RequestModel requestModel, Model model) {

		model.addAttribute("weatherModel", weatherAdapter.retriveWeather(requestModel));

		return "weather";
	}

}
