<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<html>
<head>
<title>First spring web-app</title>
<style type="text/css">
body {
	background-color: wheat;
}
</style>
</head>
<body>

	<h2>Weather</h2>
	<form:form method="POST" action="/weather/search" modelAttribute="requestModel">
		<table>
			<tr>
				<td><form:label path="name">Name</form:label></td>
				<td><form:input path="name" /></td>
			</tr>
			<tr>
				<td><form:label path="kelvin">Kelvin or Celsius?</form:label>
				<td><form:radiobutton path="kelvin" value="true"/>Kelvin</td>
				<td><form:radiobutton path="kelvin" value="false"/>Celsius</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>


	<h1>Hello ${weatherModel.cityName}</h1>

	<h1>${weatherModel.temperature} °C</h1>

</body>
</html>